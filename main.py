import os
from getpass import getuser
import requests


#os.system('sudo apt install python3-pip')

while True:
    entrada = int(input('''Menù de opciones:
0. Para salir.
1. Ver si tienes docker instalado.
2. Actualizar el sistema.
3. Instalar docker.
4. Instalar ssh.
5. Instalar java.
6. Instalar docker-compose.
7. Instalar nginx.
8. Instalar vim.
9. Instalar Visual Studio Code.
10. Instalar Sublime Text 3.
11. Instalar Google Chrome.
12. Instalar Brave.
13. Personalizar la terminal.
14. Instalar Packet Tracer.
'''))

    user = getuser()

    if entrada == 0:
        break
    elif entrada == 1:
        command = 'docker version'
        os.system(command)
        print()
    elif entrada == 2:
        command = 'sudo apt update; sudo apt upgrade -y'
        os.system(command)
        print()
    elif entrada == 3:
        file = open('requeriments/docker.txt', 'r')
        dato = file.read()
        datos = dato.split(';')
        for i in datos:
            if i == 'sudo docker -aG docker':
                user_docker = i + user
                os.system(user_docker)
            else:
                os.system(i)
        print()
    elif entrada == 4:
        command = 'sudo apt install openssh-server openssh-client'
        os.system(command)
        print()
    elif entrada == 5:
        file = open('requeriments/java.txt','r')
        dato = file.read()
        datos = dato.split(';')
        for i in datos:
            os.system(i)
        print()
    elif entrada == 6:
        file = open('requeriments/docker-compose.txt')
        dato = file.read()
        datos = dato.split(';')
        for i in datos:
            os.system(i)
        print()
    elif entrada == 7:
        file = open('requeriments/nginx.txt','r')
        dato = file.read()
        datos = dato.split(';')
        for i in datos:
            os.system(i)
        print()
    elif entrada == 8:
        command = 'sudo apt install vim -y'
        os.system(command)
        print()
    elif entrada == 9:
        command = 'sudo dpkg -i code_1.53.2-1613044664_amd64.deb'
        os.system(command)
        print()
    elif entrada == 10:
        file = open('requeriments/sublime_text.txt','r')
        dato = file.read()
        dato = dato.split(';')
        for i in datos:
            os.system(i)
        print()
    elif entrada == 11:
        command = 'sudo dpkg -i google-chrome-stable_current_amd64.deb'
        os.system(command)
        print()
    elif entrada == 12:
        file = open('requeriments/brave.txt','r')
        dato = file.read()
        datos = dato.split(';')
        for i in datos:
            os.system(i)
        print()
    elif entrada == 13:
        file = open('requeriments/shell.txt','r')
        dato = file.read()
        datos = dato.split(';')
        for i in datos:
            os.system(i)
        print()
        print('Para ver los temas, ve aqui https://github.com/ohmybash/oh-my-bash/wiki/Themes')
    elif entrada == 14:
        file = open('requeriments/packet_tracer.txt','r')
        dato = file.read()
        datos = dato.split(';')
        for i in datos:
            os.system(i)
        print()
    else:
        print('Esta no es una opciòn')
        print()
